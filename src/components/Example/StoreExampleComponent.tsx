import React, {FunctionComponent, ReactElement} from 'react';
import {IExampleItem} from "../../types/ExampleItem";

interface IProps {
    elements: IExampleItem[];
}

export const StoreExampleComponent: FunctionComponent<IProps> = props => {
    return (
        <div>
            {props.elements.map((element: IExampleItem, i: number): ReactElement => <p key={i}>{i} - {element.name}</p>)}
        </div>
    )
};