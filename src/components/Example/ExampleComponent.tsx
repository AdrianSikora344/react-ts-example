import React, {FunctionComponent} from 'react';

interface IProps {
    content: string;
}

export const ExampleComponent: FunctionComponent<IProps> = props => {
    return (
        <div>
            <p>Example component - {props.content}</p>
        </div>
    )
};