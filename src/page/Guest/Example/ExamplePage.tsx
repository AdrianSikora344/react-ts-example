import React, {FunctionComponent} from 'react';
import {ExampleContainer} from "../../../containers/Example/Example";

export const ExamplePage: FunctionComponent = () => {
    return (
        <div>
            <h3>Example page</h3>

            <ExampleContainer/>
        </div>
    )
};