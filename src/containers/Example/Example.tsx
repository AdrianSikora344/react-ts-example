import React, {FunctionComponent} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {IAppState} from "../../store/configure-store";

import {IExampleItemReducer} from "../../store/reducers/example-item";
import {addExampleItem} from "../../store/actions";

import {ExampleComponent} from '../../components/Example/ExampleComponent';
import {StoreExampleComponent} from "../../components/Example/StoreExampleComponent";
import {IExampleItem} from "../../types/ExampleItem";

export const ExampleContainer: FunctionComponent = () => {
    const dispatch = useDispatch();
    const exampleItems: IExampleItemReducer = useSelector((store: IAppState) => store.exampleItem);

    const handleClick = (): void => {
        const element: IExampleItem = {
            id: "id", name: "Element"
        };

        dispatch(addExampleItem(element));
    };

    return (
        <div>
            <ExampleComponent content="Example props value"/>
            <StoreExampleComponent elements={exampleItems.items}/>

            <button onClick={handleClick}>Dodaj element</button>
        </div>
    )
};