import React, {FunctionComponent} from 'react';
import './App.css';

import {
    ExamplePage
} from './page';

const App: FunctionComponent = () => {
    return (
        <div className="App">
            <ExamplePage/>
        </div>
    );
};

export default App;
