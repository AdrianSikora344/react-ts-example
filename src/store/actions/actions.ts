import {IExampleItemActionTypes} from "./example-item";

export enum AppEvents {
    SET_EXAMPLE_ITEMS = "APP/SET_EXAMPLE_ITEMS",
    ADD_EXAMPLE_ITEM = "APP/ADD_EXAMPLE_ITEM"
}

export type IAppActions = IExampleItemActionTypes;