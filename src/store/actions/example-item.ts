import {AppEvents} from "./actions";
import {IExampleItem} from "../../types/ExampleItem";

export interface ISetExampleItemsAction {
    type: typeof AppEvents.SET_EXAMPLE_ITEMS;
    items: IExampleItem[];
}

export interface IAddExampleItemAction {
    type: typeof AppEvents.ADD_EXAMPLE_ITEM;
    item: IExampleItem;
}

export type IExampleItemActionTypes = ISetExampleItemsAction | IAddExampleItemAction;

export const setExampleItems = (items: IExampleItem[]): ISetExampleItemsAction => ({
    type: AppEvents.SET_EXAMPLE_ITEMS,
    items: items
});

export const addExampleItem = (item: IExampleItem): IAddExampleItemAction => ({
    type: AppEvents.ADD_EXAMPLE_ITEM,
    item: item
});