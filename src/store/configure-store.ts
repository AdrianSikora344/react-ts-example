import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';

import {exampleItemReducer} from './reducers/example-item';

const rootReducers = combineReducers({
    exampleItem: exampleItemReducer
});

export type IAppState = ReturnType<typeof rootReducers>;

export const configureStore = () => {
//    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const composeEnhancers = compose;

    return createStore(rootReducers, composeEnhancers(applyMiddleware(thunk)));
};