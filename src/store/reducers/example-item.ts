import {Reducer} from "react";
import {AppEvents} from "../actions/actions";
import {IExampleItem} from "../../types/ExampleItem";
import {IExampleItemActionTypes} from "../actions/example-item";

export interface IExampleItemReducer {
    isFetching: boolean;
    items: IExampleItem[];
}

const initialState: IExampleItemReducer = {
    isFetching: false,
    items: []
};

export const exampleItemReducer: Reducer<IExampleItemReducer, IExampleItemActionTypes> = (state = initialState, action) => {
    switch (action.type) {
        case AppEvents.SET_EXAMPLE_ITEMS:
            return {
                ...state,
                items: action.items,
            };

        case AppEvents.ADD_EXAMPLE_ITEM:
            return {
                ...state,
                items: [...state.items, action.item],
            };

        default:
            return state
    }
};