React TypeScript template

### Installed packages:
npm install --save redux
npm install --save-dev @types/redux

npm install --save react-redux
npm install --save-dev @types/react-redux

npm install --save redux-thunk
npm install --save-dev @types/redux-thunk

npm install --save moment
npm install --save axios

npm install --save-dev eslint
npm install --save-dev @typescript-eslint/parser
npm install --save-dev @typescript-eslint/eslint-plugin